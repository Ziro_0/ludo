import { Component } from '@angular/core';
import { Platform, MenuController, NavController, NavParams } from 'ionic-angular';
import { Game as GameLudo } from '../../game-ludo/game';
import { PlayerColors } from '../../game-ludo/classes/LudoEnums';
import ServerPlayerData from '../../game-ludo/classes/ServerPlayerData';
import MapData from '../../game-ludo/classes/MapData';
import { irandomRange } from '../../game-ludo/util/util';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    showAlertMessage: boolean = true;
    autoplayTimer: any = null;
    isDemo: boolean = true;
    isIos: boolean = false;
    isTutorial: boolean = false;
    finalStageNumber: number = 7;
    tutorialStage: number = 1;
    visibleState: string = 'visible';
    randomLeft: number = 0;
    opponentPoints: number = 0;
    opponentPointsLoading: boolean = true;
    bottomPosition: string = '-100px';
    staticTimerFn: any = null;
    internetAvailable: boolean = true;
    isBumper: boolean = false;
    bumperType: any = 1;
    problemText: string = null;
    loadingController: any = null;
    singleNav: any = null;
    resultsArray: any = {
        rightbubbles: 0,
        wrongbubbles: 0,
        rightTreats: 0,
        wrongTreats: 0,
        totalbubbles: 0
    };
    livesRemaining: boolean = false;
    resultDeclared: boolean = false;
    persistantLives: number = 10;
    perishableLives: number = 5;

    //Change bubble bottom position
    changeBottomIndex: number = 0;

    bubbles: any = [];
    bubblesSorted: any = [];
    declaredResultData: any = {};
    popIndex: number = 0;

    gamingIndex: number = 0;

    //Timer seconds
    timerNum: number = 60;
    points: any = 0;
    bumperPoints: number = 0;
    onEnded: boolean = false;
    onLeft: boolean = false;
    problemsOver: boolean = false;
    chosenColor: string = 'red';
    bubblePopRightIndex = 0;
    bubblePopWrongIndex = 0;
    level = 0;
    boxWidth = 0;
    boxHeight = 0;
    bubbleWidthParameter = 0;
    tableMoney = 5;
    levelIncreasing: boolean = true;
    freshlyLoaded: boolean = true;
    showLoading: boolean = false;

    staticTimer: number = 2;
    staticTimerBubbleFn: any;

    //For demo
    config: any = {};
    levelConfig: any = {
    };

    ludoConfig: any = {
        assetsBaseRelativePath: 'assets/',
        assetsDataRelativePath: 'game-ludo/data/',
        
        assetsImagesRelativePath: 'game-ludo/images/',
        assetsDefaultImagesExt: '.png',

        assetsAudioRelativePath: 'game-ludo/sounds/',
        assetsDefaultAudioExts: [ '.ogg', '.mp3' ],

        diceValuesToStart: [ 1, 6 ],
        diceValuesToContinueTurn: [ 6 ],
        
        endOfTurnDelayMs: 750,

        gameBackgroundKey: 'background',

        players: [
            {
                name: "Blue",
                color: PlayerColors.BLUE
            },

            /*
            {
                name: "Red",
                color: PlayerColors.RED
            },
            */

            {
                name: "Green",
                color: PlayerColors.GREEN
            }/*,

            {
                name: "Yellow",
                color: PlayerColors.YELLOW
            }*/
        ],

        usesBackendServer: true
    };

    gameInstance: GameLudo;
    gameStyle: any = {
        width: '100%',
        height: '100%'
    };
    paramsGameId: any;

    state: any = {
        turnPlayerIndex: 0,
        diceValue: 0,
        lastPlayerMove: new Date(),
        
        willTurnPlayerContinue: false,

        /**
         * @type {MapData}
         */
        mapData: null,

        /**
         * @type {ServerPlayerData[]}
         */
        players: []
    };

    constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public menu: MenuController) {
        this.paramsGameId = navParams.get('paramsGameId');
    }

    ionViewDidLoad() {
        this.calculateBoxWidth();
        this.startNow(this.ludoConfig.startTimeSecs);
    }

    perfectCallback(gameInstance) {
        console.log('perfectCallback called in home.ts!');
        gameInstance.showPerfect();
    }

    addPointCallback(points) {
       console.log('Adding a point', points);
       this.points = this.points + points;
    }

    //===================================
    //---- BEGIN LUDO-SPECIC SERVER-CIDE GAME LOGIC
    //===================================

    bumpPlayer(playerIndex: number) {
        const playerData: ServerPlayerData = this.state.players[playerIndex];
        playerData.tileIndexPathArrayIndex = -1;
        this.gameInstance.bumpPlayer(playerIndex);
    }

    continueTurnPlayer() {
        this.setTurnPlayer(this.state.turnPlayerIndex, false);
    }

    handlePlayerCollisions(playerIndex: number) {
        const offendingPlayerData: ServerPlayerData = this.state.players[playerIndex];
        const offendingnPlayerMapTileIndex =
            offendingPlayerData.tileIndexPath[offendingPlayerData.tileIndexPathArrayIndex];

        let wereAnyPlayersBumped: boolean = false;

        this.state.players.forEach(function(defendingPlayerData: ServerPlayerData,
        defendingPlayerIndex: number) {
            if (playerIndex === defendingPlayerIndex) {
                //turn player cant bump self
                return;
            }

            const defendingPlayerMapTileIndex =
                defendingPlayerData.tileIndexPath[defendingPlayerData.tileIndexPathArrayIndex];

            if (defendingPlayerMapTileIndex !== offendingnPlayerMapTileIndex) {
                //turn player did not land in the same spot as the non-turn player
                return;
            }

            if (!defendingPlayerData.isSafe) {
                wereAnyPlayersBumped = true;
                this.bumpPlayer(defendingPlayerIndex);
                this.state.willTurnPlayerContinue = true;
            }
        }, this);

        return(wereAnyPlayersBumped);
    }

    handlePlayerStarted(playerIndex: number) {
        const playerData: ServerPlayerData = this.state.players[playerIndex];
        playerData.tileIndexPathArrayIndex = 0;
        this.movePlayerToStart(playerIndex);
    }

    handlePlayerWon(playerIndex: number) {
        const playerData: ServerPlayerData = this.state.players[playerIndex];
        console.log(`player ${playerData.color} wins!`);
        this.gameInstance.handlePlayerWon(playerIndex);
    }

    handleTurnPlayerInvalidRoll() {
    }

    hasTurnPlayerTokenStarted() {
        return(this.turnPlayerData.tileIndexPathArrayIndex > -1);
    }

    isRollStartable(rollValue: number): boolean {
        const diceValuesToStart: number[] = this.ludoConfig.diceValuesToStart;
        return(diceValuesToStart.indexOf(rollValue) > -1);
    }

    isThisPlayersTurn(playerIndex: number): boolean {
        const turnPlayerIndex: number = this.state.turnPlayerIndex;
        if (playerIndex !== turnPlayerIndex) {
            console.trace(
                `Wrong player's turn. Expected index ${turnPlayerIndex}; got index ${playerIndex}`);
            return(false);
        }
        return(true);
    }

    ludoEngineListener() {
        this.gameInstance = new GameLudo(this.boxWidth, this.boxHeight);
        this.gameInstance.startGame(this.state, this.ludoConfig);

        this.gameInstance.listen('ready', this.onReady.bind(this));

        this.gameInstance.listen('dice-clicked', this.onDiceClicked.bind(this));
        this.gameInstance.listen('dice-rolling-stopped', this.onDiceRollingStopped.bind(this));
        this.gameInstance.listen('game-over', this.onGameOver.bind(this));
        this.gameInstance.listen('player-turn-finished', this.onPlayerTurnFinished.bind(this));
        this.gameInstance.listen('token-finishes', this.onTokenFinishes.bind(this));
        this.gameInstance.listen('token-gets-out', this.onTokenGetsOut.bind(this));
        this.gameInstance.listen('token-invalid-move', this.onTokenInvalidMove.bind(this));
        this.gameInstance.listen('token-jump', this.onTokenJump.bind(this));
        this.gameInstance.listen('token-stop', this.onTokenStop.bind(this));
        this.gameInstance.listen('token-sent-back', this.onTokenSentBack.bind(this));

        //set the dice value to some initial, valid value
        this.setDiceValue();
    }

    ludoSync() {
        this.gameInstance.ludoSync(this.state);
    }

    movePlayer(playerIndex: number, steps: number): boolean {
        const playerData: ServerPlayerData = this.state.players[playerIndex];
        const newTileIndex = playerData.tileIndexPathArrayIndex + steps;
        if (newTileIndex >= playerData.tileIndexPath.length) {
            return(false);
        }

        playerData.tileIndexPathArrayIndex = newTileIndex;
        this.gameInstance.moveToken(playerIndex, steps);
        return(true);
    }

    movePlayerToStart(playerIndex: number) {
        const playerData: ServerPlayerData = this.state.players[playerIndex];
        playerData.tileIndexPathArrayIndex = 0;
        this.gameInstance.moveTokenToStart(playerIndex);
    }

    onDiceClicked(gameInstance: any, playerIndex: number) {
        if (!this.isThisPlayersTurn(playerIndex)) {
            return;
        }

        const diceValue = this.setDiceValue();
        this.state.willTurnPlayerContinue =
            this.ludoConfig.diceValuesToContinueTurn.indexOf(diceValue) > -1;
        
        this.gameInstance.roll(playerIndex, diceValue);
    }

    onDiceRollingStopped(gameInstance, playerIndex: number, rollValue: number) {
        //verify that the specified player is in fact the turn player
        if (!this.isThisPlayersTurn(playerIndex)) {
            return;
        }

        //verify correct dice roll value
        if (rollValue !== this.state.diceValue) {
            console.warn(`Wrong dice roll value. Expected ${this.state.diceValue}; got ${rollValue}`);
            this.setNextTurnPlayer();
            return;
        }

        if (this.hasTurnPlayerTokenStarted()) {
            if (this.movePlayer(playerIndex, rollValue)) {
                return;
            }
        } else {
            if (this.isRollStartable(rollValue)) {
                this.handlePlayerStarted(playerIndex);
                return;
            } else {
                this.handleTurnPlayerInvalidRoll();
            }
        }

        this.turnFinished(playerIndex);
    }

    onGameOver(gameInstance) {
        console.log('onGameOver');
    }

    onPlayerTurnFinished(gameInstance, playerIndex: number) {
        this.turnFinished(playerIndex);
    }

    onReady(gameInstance) {
        console.log('the game has finished loading and is ready to play');

        //when THIS player joins the game, they are assigned a unique player index.
        //The index must match up with their index in the (server) state.players array.
        //TODO: Server will need to figure this out; for now, it's hard-coded to 0.
        this.registerThisPlayerIndex(gameInstance, 0);

        if (this.ludoConfig.usesBackendServer) {
            this.setTurnPlayer(0, false);
        }
    }

    onTokenFinishes(gameInstance, playerIndex: number) {
        console.log('onTokenFinishes');
    }
        
    onTokenGetsOut(gameInstance, playerIndex: number) {
        console.log('onTokenGetsOut');
    }

    onTokenInvalidMove(gameInstance, playerIndex: number) {
        console.log('onTokenInvalidMove');
    }

    onTokenJump(gameInstance, playerIndex: number) {
        console.log('onTokenJump');
    }

    onTokenSentBack(gameInstance, playerIndexSentBack: number) {
    }

    onTokenStop(gameInstance, playerIndex: number) {
        if (!this.isThisPlayersTurn(playerIndex)) {
            return;
        }

        this.updateIsSafeStateForPlayer(playerIndex);

        //check if player collided with another player
        if (this.handlePlayerCollisions(playerIndex)) {
            return;
        }

        //check if player won
        const playerData: ServerPlayerData = this.state.players[playerIndex];
        if (playerData.tileIndexPathArrayIndex === playerData.tileIndexPath.length - 1) {
            this.handlePlayerWon(playerIndex);
            return;
        }

        this.turnFinished(playerIndex);
    }
    
    registerThisPlayerIndex(gameInstance, playerIndex: number) {
        gameInstance.registerThisPlayerIndex(playerIndex);
    }

    setDiceValue(value?: number): number {
        this.state.diceValue = value === undefined ?
            irandomRange(1, 6) :
            value;
        return(this.state.diceValue);
    }

    setNextTurnPlayer() {
        const newTurnPlayerIndex =
            (this.state.turnPlayerIndex + 1) % this.state.players.length;
        this.setTurnPlayer(newTurnPlayerIndex);
    }

    setTurnPlayer(playerIndex: number, isNewPlayersTurn: boolean = true) {
        this.state.turnPlayerIndex = playerIndex;
        this.gameInstance.setTurnPlayer(playerIndex, isNewPlayersTurn);
    }

    turnFinished(playerIndex: number) {
        if (!this.isThisPlayersTurn(playerIndex)) {
            return;
        }

        const that: any = this;
        setTimeout(function() {
            if (that.state.willTurnPlayerContinue) {
                that.state.willTurnPlayerContinue = false;
                that.continueTurnPlayer();
            } else {
                that.setNextTurnPlayer();
            }
        }, this.ludoConfig.endOfTurnDelayMs);
    }

    get turnPlayerIndex(): number {
        return(this.state.turnPlayerIndex);
    }

    get turnPlayerData(): ServerPlayerData {
        return(this.state.players[this.turnPlayerIndex]);
    }

    updateIsSafeStateForPlayer(playerIndex: number) {
        const playerData: ServerPlayerData = this.state.players[playerIndex];
        const mapData: MapData = this.state.mapData;
        const playerTileIndexex =
            playerData.tileIndexPath[playerData.tileIndexPathArrayIndex];

        playerData.isSafe = 
            mapData.safeZoneTileIndexes.indexOf(playerTileIndexex) > -1;
    }

    updateState(state: any) {
        this.state = state;
    }

    updateStateProperty(key: string, value: any) {
        if (!this.state) {
            this.updateState({ });
        }

        this.state[key] = value;
    }

    //===================================
    //---- END LUDO-SPECIC SERVER-CIDE GAME LOGIC
    //===================================

    startNow(num) {
        this.gameStyle = {
            width: this.boxWidth + 'px',
            height: this.boxHeight + 'px'
        };

        switch (this.paramsGameId) {
            case 19:
                this.ludoEngineListener();
                break;
        }

        //Information to be used!
        this.showAlertMessage = false;
        this.onEnded = false;
        this.onLeft = false;

        this.timerNum = num; //Value of the timer
        this.perishableLives = 5; //Initial per contest lives
        this.persistantLives = 0; //Final bought lives

        this.points = 0; //Points scored

        this.checkLivesAvailable(); //This function checks if lives are available or not! It ends your contest if your lives are over.
    }

    checkLivesAvailable() {
        if (this.perishableLives <= 0 && this.persistantLives <= 0) {
            this.livesRemaining = false;
            this.onEnd();
        } else {
            this.livesRemaining = true;
        }
    }

    calculateBoxWidth() {
        this.boxWidth = this.platform.width();
        this.boxHeight = this.platform.height();

        // if (this.isIos) {
        //     this.boxHeight = this.boxHeight - 50;
        // }
    }

    problemsOverFn(problemText?) {
        this.problemText = 'Some problem occurred, Please try again!';
        if (problemText) {
            this.problemText = problemText;
        }
        this.timerNum = 0;
        this.problemsOver = true;
        this.clearTimer();
    }

    nextStage() {
        this.tutorialStage = this.tutorialStage + 1;

        if (this.tutorialStage == 4) {
            this.perishableLives = 0;
        }
        if (this.tutorialStage == 5) {
            this.persistantLives = 0;
        }

        if (this.tutorialStage == this.finalStageNumber + 1) {
            this.dismiss();
        }
    }

    chooseLevel() {
        if (this.level < this.config.levels + 1 && this.levelIncreasing) {
            this.level++;
        }

        if (this.level > 0 && !this.levelIncreasing) {
            this.level--;
        }

        if (this.level == 0) {
            this.level = 2;
            this.levelIncreasing = true;
        }

        if (this.level == this.config.levels + 1) {
            this.level = this.config.levels - 1;
            this.levelIncreasing = false;
        }

        this.levelConfig = this.config[this.level];
    }

    generateRandom(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    dismiss() {
        this.navCtrl.pop();
    }

    confirmDismissal() {
        this.showAlertMessage = false;
        this.onEnded = true;
        this.onLeft = true;
        this.clearTimer();

        setTimeout(() => {
            this.clearTimer();
        }, 1000);

        this.menu.enable(true);
    }

    clearTimer() {
        if (this.staticTimerFn) {
            clearTimeout(this.staticTimerFn);
            this.staticTimerFn = null;
        }
    }

    ionViewDidEnter() {
        this.menu.enable(false);
    }

    ionViewCanLeave() {
        if (!this.onEnded && !this.problemsOver && !this.isTutorial) {
        } else {
            this.menu.enable(true);
        }
    }

    getBubbleParamsAccToWidth() {
        let width = this.boxWidth; //width of box
        let singleBoxParams = {};
        this.levelConfig.box.forEach(singleBox => {
            if (singleBox.minWidth) {
                if (singleBox.maxWidth) {
                    //Check width between minWidth and maxWidth
                    if (width >= singleBox.minWidth && width <= singleBox.maxWidth) {
                        singleBoxParams = singleBox;
                    }
                } else {
                    //Check width >= minWidth
                    if (width >= singleBox.minWidth) {
                        singleBoxParams = singleBox;
                    }
                }
            } else {
                //Check width between 0 and maxWidth
                if (width >= 0 && width <= singleBox.maxWidth) {
                    singleBoxParams = singleBox;
                }
            }
        });

        return singleBoxParams;
    }

    _arrayRandom(len, min, max, unique) {
        var len = len ? len : 10,
            min = min !== undefined ? min : 1,
            max = max !== undefined ? max : 100,
            unique = unique ? unique : false,
            toReturn = [],
            tempObj = {},
            i = 0;

        if (unique === true) {
            for (; i < len; i++) {
                var randomInt = Math.floor(Math.random() * (max - min + 1)) + min;
                if (tempObj['key_' + randomInt] === undefined) {
                    tempObj['key_' + randomInt] = randomInt;
                    toReturn.push(randomInt);
                } else {
                    i--;
                }
            }
        } else {
            for (; i < len; i++) {
                toReturn.push(Math.floor(Math.random() * (max - min + min)));
            }
        }

        return toReturn;
    }

    removeLoading() {
        if (this.showLoading) {
            this.showLoading = false;
            this.loadingController.dismiss();
        }
    }

    onTimerFinished() {
        this.onEnd();
    }

    onEnd() {
        if (!this.onEnded) {
            this.gameInstance.endGame();
        }
        this.clearTimer();
        this.onEnded = true;
        this.onLeft = true;

        if (this.isBumper || this.isDemo) {
            if (this.livesRemaining) {
                this.dismiss();
            }
        } else {
            if (this.livesRemaining) {
            }
        }
    }
}
