export function irandom(max) {
    return Math.floor(Math.random() * (max + 1));
}

export function irandomRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function randomRange(min, max) {
    return Math.random() * (max - min) + min;
}

export function choose(...items) {
    if (items.length === 0) throw new Error('You need to pass at least one item');
    return items[irandom(items.length - 1)];
}

export function checkOverlap(spriteA, spriteB) {
    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();

    return Phaser.Rectangle.intersects(boundsA, boundsB);
}
