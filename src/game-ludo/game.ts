// import pixi, p2 and phaser ce
import 'pixi';
import 'p2';
import * as Phaser from 'phaser-ce';

// import states
import BootState from './states/BootState';
import PreloadState from './states/PreloadState';
import GameState from './states/GameState';
import SetupServerLikeState from './states/SetupServerLikeState';

/**
 * Main entry game class
 * @export
 * @class Game
 * @extends {Phaser.Game}
 */
export class Game extends Phaser.Game {
    config: any;
    serverState: any;
    listenerMapping: any = {};

    /**
     * Creates an instance of Game.
     * @memberof Game
     */
    constructor(width, height) {
        // call parent constructor
        // super(width, height, Phaser.CANVAS, 'game', null);
        super(720, 1280, Phaser.CANVAS, 'game', null);

        // add some game states
        this.state.add('BootState', new BootState(this));
        this.state.add('PreloadState', new PreloadState(this));
        this.state.add('SetupServerLikeState', new SetupServerLikeState(this));
        this.state.add('GameState', new GameState(this));
    }

    bumpPlayer(playerIndex: number) {
        const state: any = this.state.getCurrentState();
        if (state) {
            state.sendTokenBack(playerIndex);
        }
    }

    endGame() {
        this.paused = true;
        setTimeout(() => {
            this.destroy();
        }, 100);
    }

    handlePlayerWon(playerIndex: number) {
        const state: any = this._getCurrentGameState();
        if (state) {
            state.presentWinningPlayer(playerIndex);
        }
    }

    listen(listenValue: string, callback: Function) {
        this.listenerMapping[listenValue] = callback;
    }

    ludoSync(serverState) {
        const state: any = this._getCurrentGameState();
        if (state) {
            state.ludoSync(serverState);
        }
    }

    moveToken(playerIndex: number, steps: number) {
        const state: any = this._getCurrentGameState();
        if (state) {
            state.moveToken(playerIndex, steps);
        }
    }

    moveTokenToStart(playerIndex: number) {
        const state: any = this._getCurrentGameState();
        if (state) {
            state.moveTokenToStartFromServer(playerIndex);
        }
    }

    registerThisPlayerIndex(playerIndex: number) {
        const state: any = this._getCurrentGameState();
        if (state) {
            state.thisPlayerIndex = playerIndex;
        }
    }

    resurrect() {
        // this.core.respawn();
    }

    roll(playerIndex: number, rollValue: number) {
        const state: any = this._getCurrentGameState();
        if (state) {
            state.roll(playerIndex, rollValue);
        }
    }

    setTurnPlayer(playerIndex: number, isNewPlayersTurn: boolean = true) {
        const state: any = this._getCurrentGameState();
        if (state) {
            state.setTurnPlayer(playerIndex, isNewPlayersTurn);
        }
    }

    startGame(serverState: any, config: any) {
        console.log('game has started');
        
        this.serverState = serverState;
        this.config = config;
        
        console.log("server state:");
        console.log(this.serverState);

        console.log("config:");
        console.log(this.config);

        this.state.start('BootState');
    }

    /**
     * @private
     */
    _getCurrentGameState(): any {
        const state: any = this.state.getCurrentState();
        return(state && state.key === 'GameState' ? state : null);
    }
}
