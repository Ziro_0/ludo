export default class PreloadState {
    game: any;
    preloadBar: Phaser.Image;

    constructor(game) {
        this.game = game;
    }

    preload() {
        const logo: Phaser.Image = this.game.add.image(
            this.game.world.centerX,
            this.game.world.centerY,
            'loading-logo');
        logo.anchor.set(0.5);

        this.preloadBar = this.game.add.image(
            this.game.world.centerX - 200,
            logo.y + logo.height + 10,
            'preload_bar');

        this.game.load.setPreloadSprite(this.preloadBar);

        //load images
        this._loadImage('background');
        this._loadImage('board');
        this._loadImage('board-back-frame');
        this._loadImage('button-2p-off');
        this._loadImage('button-2p-on');
        this._loadImage('button-3p-off');
        this._loadImage('button-3p-on');
        this._loadImage('button-4p-off');
        this._loadImage('button-4p-on');
        this._loadImage('button-close');
        this._loadImage('button-credits');
        this._loadImage('button-intro-play');
        this._loadImage('button-play');
        this._loadImage('button-replay');
        this._loadImage('button-return');
        this._loadImage('button-settings');
        this._loadImage('button-sound');
        this._loadImage('dice-1');
        this._loadImage('dice-2');
        this._loadImage('dice-3');
        this._loadImage('dice-4');
        this._loadImage('dice-5');
        this._loadImage('dice-6');
        this._loadImage('dice-bg-blue');
        this._loadImage('dice-bg-green');
        this._loadImage('dice-bg-red');
        this._loadImage('dice-bg-yellow');
        this._loadImage('dice-roll-0');
        this._loadImage('dice-roll-1');
        this._loadImage('dice-roll-2');
        this._loadImage('button-home');
        this._loadImage('intro-background');
        this._loadImage('medal-0');
        this._loadImage('medal-1');
        this._loadImage('medal-2');
        this._loadImage('menu-background');
        this._loadImage('particle0');
        this._loadImage('particle1');
        this._loadImage('particle2');
        this._loadImage('particle3');
        this._loadImage('play-button-background');
        this._loadImage('safe-zone-icon');
        this._loadImage('settings-panel');
        this._loadImage('title');
        this._loadImage('token-blue-1');
        this._loadImage('token-green-1');
        this._loadImage('token-red-1');
        this._loadImage('token-selected-frame');
        this._loadImage('token-selection-frame');
        this._loadImage('token-yellow-1');
        this._loadImage('turn-player-right-arrow');
        this._loadImage('trail-blue');
        this._loadImage('trail-green');
        this._loadImage('trail-red');
        this._loadImage('trail-yellow');

        //load sounds
        this._loadAudio('button-click');
        this._loadAudio('dice');
        this._loadAudio('new-token');
        this._loadAudio('next-turn');
        this._loadAudio('option-button-click');
        this._loadAudio('player-complete');
        this._loadAudio('safe-zone');
        this._loadAudio('token-booted');
        this._loadAudio('token-complete');
        this._loadAudio('token-move');

        //load data
        this._loadJson('map-data');

        //load fonts
        this._loadFont("modenine");
    }

    create() {
        if (this.game.config.usesBackendServer) {
            this.game.state.start('SetupServerLikeState');
        } else {
            this.game.state.start('GameState');
        }
    }

    /**
     * @private
     */
    _loadAudio(key: string, urls: any = null) {
        const config = this.game.config;
      
        if (!urls) {
            urls = [];
            config.assetsDefaultAudioExts.forEach(function(ext: string) {
                urls.push(config.assetsBaseRelativePath +
                    config.assetsAudioRelativePath +
                    key +
                    ext);
            }, this);
        }

        this.game.load.audio(key, urls);
    }

    /**
     * @private
     */
    _loadFont(key: string, textureUrl: string = null, atlasUrl: string = null) {
        const config = this.game.config;
        
        if (!textureUrl) {
            textureUrl = config.assetsBaseRelativePath +
                config.assetsImagesRelativePath +
                key +
                config.assetsDefaultImagesExt;
        }

        if (!atlasUrl) {
            atlasUrl = config.assetsBaseRelativePath +
                config.assetsDataRelativePath +
                key +
                ".xml";
        }

        this.game.load.bitmapFont(key, textureUrl, atlasUrl);
    }

    /**
     * @private
     */
    _loadImage(key: string, url: string = null) {
        const config = this.game.config;
      
        if (!url) {
            url = config.assetsBaseRelativePath +
                config.assetsImagesRelativePath +
                key +
                config.assetsDefaultImagesExt;
        }

        this.game.load.image(key, url);
    }

    /**
     * @private
     */
    _loadJson(key: string, url: string = null) {
        const config = this.game.config;
      
        if (!url) {
            url = config.assetsBaseRelativePath +
                config.assetsDataRelativePath +
                key + ".json";
        }

        this.game.load.json(key, url);
    }
}
