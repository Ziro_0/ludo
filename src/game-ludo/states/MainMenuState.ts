import StartButton from "../classes/StartButton";

export default class MainMenuState {
    game: any;

    constructor(game) {
        this.game = game;
    }

    create() {
        console.log('MainMenu state [create] started');
        this._createBackgroundImage();
        this._createTitleImage();
        this._createPlayButton();
    }

    /**
     * @private
     */
    _createBackgroundImage() {
        const world = this.game.world;
        let bgImage = this.game.add.image(0, 0, 'intro-background');
        bgImage.width = world.width;
        if (bgImage.height < world.height) {
            bgImage.height = world.height;
            bgImage.scale.x = bgImage.scale.y;
        } else {
            bgImage.scale.y = bgImage.scale.x;
        }
    }

    /**
     * @private
     */
    _createPlayButton() {
        let startButton = new StartButton(this.game);
        startButton.onPressed.addOnce(this._onPlayButtonPressed, this);
    }

    /**
     * @private
     */
    _createTitleImage() {
        const world = this.game.world;
        let title = this.game.add.image(0, 0, 'title');
        title.anchor.set(0.5);
        title.x = world.centerX;
        title.y = world.height * 0.15;
        title.width = world.width * 0.9;
        title.scale.y = title.scale.x;
    }

    /**
     * @private
     */
    _onPlayButtonPressed() {
        this.game.state.start('GameState');
    }
}
