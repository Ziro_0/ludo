export default class BootState {
    game: any;

    constructor(game) {
        this.game = game;
    }

    init() {
        // Responsive scaling
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        // Center the game
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;
    }

    preload() {
        const load = this.game.load;
        load.image('preload_bar', 'assets/game-ludo/images/preload_bar.png');
        load.image('loading-logo', 'assets/game-ludo/images/loading-logo.png');
    }

    create() {
        this.game.state.start('PreloadState');
    }
}
