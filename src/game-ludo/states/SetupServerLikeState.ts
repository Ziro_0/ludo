import { Game } from "phaser-ce";
import { PlayerColors } from "../classes/LudoEnums";
import BoardMath from "../classes/BoardMath";
import MapData from "../classes/MapData";
import ServerPlayerData from "../classes/ServerPlayerData";

export default class SetupServerLikeState {
    game: any;

    constructor(game: Game) {
        this.game = game;
    }

    create() {
        this._setupState();
        this.game.state.start('GameState');
    }

    /**
     * @private
     */
    _buildPlayerTileIndexPath(color: PlayerColors) {
        const mapData: MapData = this.game.serverState.mapData;
        return(BoardMath.BuildPlayerTileIndexPath(mapData, color));
    }
    
    /**
     * @private
     */
    _initMapData() {
        const jMapData = this.game.cache.getJSON('map-data');
        return(new MapData(jMapData));
    }

    /**
     * @private
     */
    _setupState() {
        const config: any = this.game.config;
        const serverState: any = this.game.serverState;
        
        //init the map data from the json file
        serverState.mapData = this._initMapData();

        //setup the players
        config.players.forEach(function(player: any, index: number) {
            const name: string = player.name
            const color: PlayerColors = player.color;

            const serverPlayerData = new ServerPlayerData();
            serverPlayerData.name = name || "Player " + (index + 1);
            serverPlayerData.color = color;
            
            //build the player's unique path along the map
            const pathData = this._buildPlayerTileIndexPath(color);
            serverPlayerData.tileIndexPath = pathData.tileIndexPath;
            serverPlayerData.endPathStartIndex = pathData.endPathStartIndex;

            serverState.players.push(serverPlayerData);
        }, this);
    }
}
