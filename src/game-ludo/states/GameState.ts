import PlayerData from '../classes/PlayerData';
import Board from '../classes/Board';
import { Game } from '../game';
import { PlayerColors } from '../classes/LudoEnums';
import DicePanels from '../classes/DicePanels';
import TurnPlayerArrows from '../classes/TurnPlayerArrows';
import PlayerTokenMover from '../classes/PlayerTokenMover';
import BoardMath from '../classes/BoardMath';
import ServerPlayerData from '../classes/ServerPlayerData';

export default class GameState {
    game: Game;
    thisPlayerIndex: number;

    _playerData: Map<PlayerColors, PlayerData>;

    _turnPlayerColorOrder: PlayerColors[];
    
    _board: Board;
    _dicePanels: DicePanels;
    _turnPlayerArrows: TurnPlayerArrows;
    _playerTokenMover: PlayerTokenMover;
    
    _backgroundImage: Phaser.Image;
    
    _winningPlayerColor: PlayerColors;
    
    _turnPlayerIndex: number;

    _canUseDice:boolean
    _willTurnPlayerContinue: boolean;
    _movingTokenJustStarted: boolean;
    _isFirstTurn: boolean;

    constructor(game: Game) {
        this.game = game;
        this._isFirstTurn = true;
    }

    continueTurnPlayer() {
        this._willTurnPlayerContinue = false;
        this.setTurnPlayer(this._turnPlayerIndex, false);
    }

    create() {
        console.log('Game state [create] started');
        this._init();
        this._callListenerMap('ready', this.game);
    }

    ludoSync(serverState: any) {
        if (!serverState) {
            console.warn('No server state specified');
            return; //sanity check
        }
        
        this.game.serverState = serverState;
        this._playerTokenMover.stop();
        this._dicePanels.diceValue = this.game.serverState.diceValue;
        this._updatePlayersDataFromState();
        this.setTurnPlayer(this.game.serverState.turnPlayerIndex);
    }

    moveToken(playerIndex: number, steps: number) {
        const color = this._turnPlayerColorOrder[playerIndex];
        if (!color) {
            console.warn('invalid playerIndex: ' + playerIndex);
            return;
        }

        let playerData = this._playerData.get(color);
        if (!playerData) {
            console.warn('invalid color: ' + color);
            return;
        }
        
        let targetMapTileIndexes: number[] = [];
        for (let index = 1; index <= steps; index++) {
            let mapTileIndex =
                playerData.tileIndexPath[playerData.tileIndexPathArrayIndex + index];
            targetMapTileIndexes.push(mapTileIndex);
        }
        
        if (this._playerTokenMover.move(color, targetMapTileIndexes)) {
            playerData.tileIndexPathArrayIndex += steps;
        }
    }

    moveTokenToStartFromServer(playerIndex: number) {
        const color = this._turnPlayerColorOrder[playerIndex];
        const playerData = this._playerData.get(color);
        const mapTileIndex = playerData.tileIndexPath[0];
        
        if (!this._playerTokenMover.move(playerData.color, mapTileIndex)) {
            return;
        }

        console.log(`player color ${color} has started!`);

        this._movingTokenJustStarted = true;
        playerData.tileIndexPathArrayIndex = 0;
    }

    presentWinningPlayer(playerIndex: number) {
        const color = this._turnPlayerColorOrder[playerIndex];
        if (!color) {
            console.warn('invalid playerIndex: ' + playerIndex);
            return;
        }
        
        if (!this.game.config.usesBackendServer) {
            this._callListenerMap('token-finishes', this.game, playerIndex);
            console.log(`player ${color} wins!`);
        }

        this._presentPlayerFanfare(color);
        this._gameOverDelay();
    }

    roll(playerIndex: number, rollValue: number) {
        const color = this._turnPlayerColorOrder[playerIndex];
        if (!color) {
            console.warn('invalid playerIndex: ' + playerIndex);
            return;
        }

        this._dicePanels.present(color);
        this._dicePanels.rollDice(rollValue);
    }

    sendTokenBack(playerIndex: number): boolean {
        const color = this._turnPlayerColorOrder[playerIndex];
        if (!this._playerTokenMover.moveToOrigin(color)) {
            return(false);
        }

        const playerData = this._playerData.get(color);
        playerData.tileIndexPathArrayIndex = -1;
        
        this._playSound('token-booted');

        return(true);
    }

    setTurnPlayer(index: number, isNewPlayersTurn: boolean = false) {
        this._turnPlayerIndex = index;
        const playerColor = this._turnPlayerColor;
        this._dicePanels.present(playerColor);
        this._turnPlayerArrows.unpresent();

        if (this.game.config.usesBackendServer) {
            if (this._turnPlayerIndex !== this.thisPlayerIndex) {
                //TODO: remove this setTimeout temp code when production-ready;
                // it's only for testing multi-player on a single machine
                //only the return call below should remain
                const that: GameState = this;
                const TEST_BOT_PLAYER_DELAY_MS = 500;
                setTimeout(function() {
                    that._callListenerMap('dice-clicked', that.game, that._turnPlayerIndex);
                }, TEST_BOT_PLAYER_DELAY_MS);

                return;
            }
        }

        this._turnPlayerArrows.present(playerColor);
        this._canUseDice = true;

        if (isNewPlayersTurn) {
            if (!this._isFirstTurn) {
                this._playSound('next-turn');
            }
        }

        this._isFirstTurn = false;
    }

    shutdown() {
        this._uninitTurnPlayerArrows();
        this._dicePanels.destroy();
        this._playerTokenMover.dispose();
    }

    /**
     * @private
     */
    _buildPlayerTileIndexPath(color: PlayerColors) {
        return(BoardMath.BuildPlayerTileIndexPath(this._board.mapData, color));
    }

    /**
     * @private
     */
    _bumpPlayer(color: PlayerColors): boolean {
        if (!this._playerTokenMover.moveToOrigin(color)) {
           return(false);
        }

        let playerData = this._playerData.get(color);
        playerData.tileIndexPathArrayIndex = -1;
        
        this._willTurnPlayerContinue = true;
        this._playSound('token-booted');
        return(true);
    }

    /**
     * @private
     */
    _callListenerMap(key: string, ...args) {
        const callback: Function = this.game.listenerMapping[key];
        if (callback) {
            callback.apply(null, args);
        }
    }

    /**
     * @private
     */
    _canTurnPlayerMoveOnEndPath(rollValue: number): boolean {
        const playerData = this._turnPlayerData;
        const targetArrayIndex = playerData.tileIndexPathArrayIndex + rollValue;
        return(targetArrayIndex <= playerData.tileIndexPath.length - 1);
    }

    /**
     * @private
     */
    _gameOverDelay() {
        const GAME_OVER_DELAY_MS = 1000;
        let timer = this.game.time.create();
        timer.add(GAME_OVER_DELAY_MS, function() {
            this._onGameOverDelayTimerComplete();
        }, this);
        timer.start();
    }
    
    /**
     * @private
     */
    _endOfTurnDelay() {
        let timer = this.game.time.create();
        timer.add(this.game.config.endOfTurnDelayMs, function() {
            this._onEndOfTurnTimerComplete();
        }, this);
        timer.start();
    }

    /**
     * @private
     */
    _handleTurnPlayerCantMoveOnEndPath() {
        console.log(`player color ${this._turnPlayerColor} cant move on end path; got a crap roll`);
        this._callListenerMap('token-invalid-move', this.game,
            this._turnPlayerColorOrder.indexOf(this._turnPlayerColor));
    }

    /**
     * @private
     */
    _handleBumpNonTurnPlayers(): boolean {
        const turnPlayerData = this._turnPlayerData;
        const turnPlayerMapTileIndex =
            turnPlayerData.tileIndexPath[turnPlayerData.tileIndexPathArrayIndex];

        let wereAnyPlayersBumped: boolean = false;

        this._playerData.forEach(function(playerData) {
            if (playerData.color === turnPlayerData.color) {
                //turn player cant bump self
                return;
            }

            const nonTurnPlayerMapTileIndex =
                playerData.tileIndexPath[playerData.tileIndexPathArrayIndex];

            if (nonTurnPlayerMapTileIndex !== turnPlayerMapTileIndex) {
                //turn player did not land in the same spot as the non-turn player
                return;
            }

            if (!playerData.isInSafeZone) {
                wereAnyPlayersBumped =
                    this._bumpPlayer(playerData.color) || wereAnyPlayersBumped;
            }
        }, this);

        return(wereAnyPlayersBumped);
    }

    /**
     * @private
     */
    _handleTurnPlayerCantStart() {
        console.log(`player color ${this._turnPlayerColor} cant start; got a crap roll`);
        this._callListenerMap('token-invalid-move', this.game);
    }
    
    /**
     * @private
     */
    _handleTurnPlayerMove(rollValue: number): boolean {
        let playerData = this._turnPlayerData;
        
        this._updateWillTurnPlayerContinueFromRoll(rollValue);

        let targetMapTileIndexes: number[] = [];
        for (let index = 1; index <= rollValue; index++) {
            let mapTileIndex =
                playerData.tileIndexPath[playerData.tileIndexPathArrayIndex + index];
            targetMapTileIndexes.push(mapTileIndex);
        }
        
        if (!this._playerTokenMover.move(playerData.color, targetMapTileIndexes)) {
            return(false);
        }

        playerData.tileIndexPathArrayIndex += rollValue;
        return(true);
    }

    /**
     * @private
     */
    _handleTurnPlayerMoveOnEndPath(rollValue: number): boolean {
        if (!this._handleTurnPlayerMove(rollValue)) {
            return(false);
        }

        this._updateWillTurnPlayerContinueFromRoll(rollValue);

        const playerData = this._turnPlayerData;
        if (playerData.tileIndexPathArrayIndex === playerData.tileIndexPath.length - 1) {
            this._winningPlayerColor = playerData.color;
        }

        return(true);
    }

    /**
     * @private
     */
    _handleTurnPlayerStartedLocal(rollValue: number):boolean {
        let playerData = this._turnPlayerData;

        console.log(`player color ${playerData.color} has started!`);

        const mapTileIndex = playerData.tileIndexPath[0];
        if (!this._playerTokenMover.move(playerData.color, mapTileIndex)) {
            return(false);
        }

        this._movingTokenJustStarted = true;
        this._updateWillTurnPlayerContinueFromRoll(rollValue);
        this._turnPlayerData.tileIndexPathArrayIndex = 0;
        return(true);
    }

    /**
     * @private
     */
    _hasTurnPlayerTokenStarted() {
        return(this._turnPlayerData.tileIndexPathArrayIndex > -1);
    }

    /**
     * @private
     */
    _init() {
        const usesBackendServer = this.game.config.usesBackendServer;

        this._initTurnPlayerOrder(usesBackendServer ?
            this.game.serverState.players :
            this.game.config.players);

        this._initBackground();
        this._initBoard();
        this._initDicePanels();
        this._initTurnPlayerArrows();

        this._initPlayerData();
        this._initPlayersOnMap();
        this._initPlayerTokenMover();

        if (!usesBackendServer) {
            this.setTurnPlayer(0);
        }
    }

    /**
     * @private
     */
    _initBackground() {
        const world = this.game.world;
        this._backgroundImage = this.game.add.image(0, 0,
            this.game.config.gameBackgroundKey);
        this._backgroundImage.width = world.width;
        if (this._backgroundImage.height < world.height) {
            this._backgroundImage.height = world.height;
            this._backgroundImage.scale.x = this._backgroundImage.scale.y;
        } else {
            this._backgroundImage.scale.y = this._backgroundImage.scale.x;
        }
    }

    /**
     * @private
     */
    _initBoard() {
        this._board = new Board(this.game);
        this._board.group.alignIn(this._backgroundImage, Phaser.CENTER);
    }

    /**
     * @private
     */
    _initDicePanels() {
        this._dicePanels = new DicePanels(this.game, this._board);
        this._dicePanels.onDicePressed.add(this._onDicePanelDicePressed, this);

        if (this.game.config.usesBackendServer) {
            this._dicePanels.onRollCompleted.add(
                this._onDicePanelRollCompletedBackend, this);
        } else {
            this._dicePanels.onRollCompleted.add(
                this._onDicePanelRollCompleted, this);
        }

    }

    /**
     * @private
     */
    _initPlayerData() {
        this._playerData = new Map();

        if (this.game.config.usesBackendServer) {
            this._initPlayerDataRemote();
        } else {
            this._initPlayerDataLocal();
        }
    }

    /**
     * @private
     */
    _initPlayerDataLocal() {
        const configPlayersData: any[] = this.game.config.players;
        configPlayersData.forEach(function(configPlayerData: any) {
            let playerData = new PlayerData(
                configPlayerData.color,
                configPlayerData.name);

            const pathData = this._buildPlayerTileIndexPath(playerData.color);
            playerData.tileIndexPath = pathData.tileIndexPath;
            playerData.endPathStartIndex = pathData.endPathStartIndex;
            
            this._playerData.set(playerData.color, playerData);
        }, this);
    }

    /**
     * @private
     */
    _initPlayerDataRemote() {
        const serverStatePlayers: ServerPlayerData[] = this.game.serverState.players;
        serverStatePlayers.forEach(function(serverStatePlayer) {
            let playerData = new PlayerData(
                serverStatePlayer.color,
                serverStatePlayer.name);

            playerData.tileIndexPath = serverStatePlayer.tileIndexPath;
            playerData.tileIndexPathArrayIndex = serverStatePlayer.tileIndexPathArrayIndex;
            playerData.endPathStartIndex = serverStatePlayer.endPathStartIndex;
            playerData.isInSafeZone = serverStatePlayer.isSafe;
            
            this._playerData.set(playerData.color, playerData);
            
            this._board.updatePlayerName(playerData);
        }, this);
    }

    /**
     * @private
     */
    _initPlayersOnMap() {
        this._playerData.forEach(function(playerData: PlayerData) {
            this._board.setupPlayer(playerData);
        }, this);
    }

    /**
     * @private
     */
    _initPlayerTokenMover() {
        this._playerTokenMover = new PlayerTokenMover(this.game, this._board);
        this._playerTokenMover.onMoveComplete.add(
            this._onPlayerTokenMoverComplete, this);
        this._playerTokenMover.onTokenStep.add(this._onPlayerTokenMoverStep, this);
    }

    /**
     * @private
     */
    _initTurnPlayerArrows() {
        this._turnPlayerArrows = new TurnPlayerArrows(this.game, this._dicePanels);
    }

    /**
     * @private
     */
    _initTurnPlayerOrder(players: any[]) {
        this._turnPlayerColorOrder = [];

        players.forEach(function(configPlayerData: any) {
            this._turnPlayerColorOrder.push(configPlayerData.color);
        }, this);
    }

    /**
     * @private
     */
    _isRollStartable(rollValue: number): boolean {
        const diceValuesToStart: number[] = this.game.config.diceValuesToStart;
        return(diceValuesToStart.indexOf(rollValue) > -1);
    }

    /**
     * @private
     */
    _isTurnPlayerOnEndPath(): boolean {
        return(this._turnPlayerData.isInEndPath);
    }

    /**
     * @private
     */
    _onDicePanelDicePressed() {
        if (!this._canUseDice) {
            return;
        }

        this._canUseDice = false;
        this._turnPlayerArrows.unpresent();

        if (this.game.config.usesBackendServer) {
            //TODO: For production, un-comment this line, and comment or remove
            // the one underneath.
            //this._callListenerMap('dice-clicked', this.game, this.thisPlayerIndex);
            
            this._callListenerMap('dice-clicked', this.game, this._turnPlayerIndex);
        } else {
            this._dicePanels.rollDice();
        }
    }

    /**
     * @private
     */
    _onDicePanelRollCompleted(rollValue: number) {
        if (this._hasTurnPlayerTokenStarted()) {
            if (this._isTurnPlayerOnEndPath()) {
                if (this._canTurnPlayerMoveOnEndPath(rollValue)) {
                    if (this._handleTurnPlayerMoveOnEndPath(rollValue)) {
                        return;
                    }
                } else {
                    this._handleTurnPlayerCantMoveOnEndPath();
                }
            } else {
                if (this._handleTurnPlayerMove(rollValue)) {
                    return;
                }
            }
        } else {
            if (this._isRollStartable(rollValue)) {
                if (this._handleTurnPlayerStartedLocal(rollValue)) {
                    return;
                }
            } else {
                this._handleTurnPlayerCantStart();
            }
        }

        this._endOfTurnDelay();
    }

    /**
     * @private
     */
    _onDicePanelRollCompletedBackend(rollValue: number) {
        //TODO: for production, uncomment this line, and remove the
        // commented or remove the one underneath

        /*
        this._callListenerMap('dice-rolling-stopped', this.game,
            this.thisPlayerIndex, rollValue);
        */

        this._callListenerMap('dice-rolling-stopped', this.game,
            this._turnPlayerIndex, rollValue);
    }

    /**
     * @private
     */
    _onEndOfTurnTimerComplete() {
        if (this._winningPlayerColor) {
            this.presentWinningPlayer(this.thisPlayerIndex);
            return;
        }

        if (this._willTurnPlayerContinue) {
            this.continueTurnPlayer();
        } else {
            this._setNextTurnPlayer();
        }
    }

    /**
     * @private
     */
    _onGameOverDelayTimerComplete() {
        this._callListenerMap('game-over', this);
        this._playSound('player-complete');
    }

    /**
     * @private
     */
    _onPlayerTokenMoverComplete(playerTokenMover: PlayerTokenMover, color: PlayerColors) {
        if (this._movingTokenJustStarted) {
            this._movingTokenJustStarted = false;
            this._playSound('new-token');
        }

        if (this.game.config.usesBackendServer) {
            //TODO: For production, un-comment this line, and comment or
            // remove the one underneath
            /*
            this._callListenerMap('token-stop', this.game, this.thisPlayerIndex);
            */

            this._callListenerMap('token-stop', this.game, this._turnPlayerIndex);
            return;
        }

        this._updatePlayerSafeZoneStatus(color);

        if (!this._handleBumpNonTurnPlayers()) {
            this._endOfTurnDelay();
        }
    }

    /**
     * @private
     */
    _onPlayerTokenMoverStep(playerTokenMover: PlayerTokenMover, color: PlayerColors) {
        const playerIndex = this._turnPlayerColorOrder.indexOf(color);
        this._callListenerMap('token-jump', this.game, playerIndex);
        this._playSound('token-move');
    }

    /**
     * @private
     */
    _playSound(key: string) {
        this.game.sound.play(key);
    }

    /**
     * @private
     */
    _presentPlayerFanfare(color: PlayerColors) {
        this._playSound('token-complete');

        const turnPlayerToken = this._board.getPlayerToken(color);
        let emitter = this.game.add.emitter(turnPlayerToken.x, turnPlayerToken.y);
        emitter.gravity.set(0);
        emitter.minParticleScale = 0.20;
        emitter.maxParticleScale = 1.00;
        emitter.minParticleSpeed.set(-200);
        emitter.maxParticleSpeed.set(200);
        this._board.mapImageGroup.add(emitter);

        const MAX_PARTICLES = 25;
        emitter.makeParticles([ 'particle0', 'particle1', 'particle2', 'particle3' ],
            null, MAX_PARTICLES);

        const PARTICLE_LIFESPAN_MS = 1000;
        emitter.explode(PARTICLE_LIFESPAN_MS);
    }

    /**
     * @private
     */
    _setNextTurnPlayer() {
        const nextTurnPlayerIndex =
            (this._turnPlayerIndex + 1) % this._turnPlayerColorOrder.length;
        this.setTurnPlayer(nextTurnPlayerIndex);
        this._playSound('next-turn');
    }

    /**
     * @private
     */
    get _turnPlayerColor(): PlayerColors {
        return(this._turnPlayerColorOrder[this._turnPlayerIndex]);
    }

    /**
     * @private
     */
    get _turnPlayerData(): PlayerData {
        return(this._playerData.get(this._turnPlayerColor));
    }

    /**
     * @private
     */
    _uninitTurnPlayerArrows() {
        if (this._turnPlayerArrows) {
            this._turnPlayerArrows.destroy();
            this._turnPlayerArrows = null;
        }
    }

    /**
     * @private
     */
    _updatePlayerSafeZoneStatus(color: PlayerColors) {
        let playerData = this._playerData.get(color);
        if (playerData) {
            let mapTileIndex =
                playerData.tileIndexPath[playerData.tileIndexPathArrayIndex];
            playerData.isInSafeZone =
                this._board.mapData.safeZoneTileIndexes.indexOf(mapTileIndex) > -1;
        }
    }

    /**
     * @private
     */
    _updateTokenLocations() {
        this._playerData.forEach(function(playerData) {
            const mapTileIndex = playerData.tileIndexPath[playerData.tileIndexPathArrayIndex];
            const tokenPoint = BoardMath.TileIndexToPixels(this._board.mapData, mapTileIndex);
            const playerToken = this._board.getPlayerToken(playerData.color);
            playerToken.x = tokenPoint.x;
            playerToken.y = tokenPoint.y;
        }, this);
    }

    /**
     * @private
     */
    _updatePlayersDataFromState() {
        this._initPlayerData();
        this._updateTokenLocations();
    }

    /**
     * @private
     */
    _updateWillTurnPlayerContinueFromRoll(rollValue: number) {
        this._willTurnPlayerContinue =
            this.game.config.diceValuesToContinueTurn.indexOf(rollValue) > -1;
    }
}
