import { PlayerColors } from "./LudoEnums";

export default class TokenTrailImage extends Phaser.Image {
    static readonly _TWEEN_DURATION_MS: number = 1000;

    constructor(game: Phaser.Game, color: PlayerColors, token: Phaser.Image) {
        const trailImageKey = `trail-${color}`;
        super(game, token.x, token.y, trailImageKey);
        
        this.anchor.set(0.5);

        token.parent.addChildAt(this, token.parent.getChildIndex(token));

        this.game.tweens.create(this.scale)
            .to({
                x: 2.0,
                y: 2.0
            },
            TokenTrailImage._TWEEN_DURATION_MS)

            .start();

        let alphaTween = this.game.tweens.create(this)
            .to({
                alpha: 0
            },
            TokenTrailImage._TWEEN_DURATION_MS)

            .start();
        
        alphaTween.onComplete.add(function() {
            this.destroy();
        }, this);
    }

    static Create(game: Phaser.Game, color: PlayerColors, token: Phaser.Image): TokenTrailImage {
        const image = new TokenTrailImage(game, color, token);
        return(image);
    }
}