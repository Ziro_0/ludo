import Board from "./Board";
import { PlayerColors } from "./LudoEnums";
import BoardMath from "./BoardMath";
import MoveTweenData from "./MoveTweenData";
import TokenTrailImage from "./TokenTrailImage";

export default class PlayerTokenMover {
    //duration, in ms, of token movement when moving about the board
    static readonly _MOVE_DURATION_MS: number = 80;
    
    //duration of the delay, in ms, of pause in between token steps
    static readonly _MOVE_DURATION_DELAY_MS: number = 10;

    //duration, in ms, of token movement when returning to the origin
    // (after being bumped by an opposing player's token)
    static readonly _MOVE_TO_ORIGIN_DURATION_MS: number = 500;

    game: Phaser.Game;
    board: Board;
    onMoveComplete: Phaser.Signal;
    onTokenStep: Phaser.Signal;
    
    _tween: Phaser.Tween;
    _currentTweenData: MoveTweenData;

    constructor(game: Phaser.Game, board: Board) {
        this.game = game;
        this.board = board;
        
        this.onMoveComplete = new Phaser.Signal();
        this.onTokenStep = new Phaser.Signal();

        this._currentTweenData = new MoveTweenData();
    }

    dispose() {
        this.onMoveComplete.dispose();
        this.onTokenStep.dispose();
    }

    move(color: PlayerColors, targetMapTileIndexes: number | number[],
    useCompleteCallback: boolean = true): boolean {
        let token = this.board.getPlayerToken(color);
        if (!token) {
            return(false);  //sanity check
        }
        
        let _targetMapTileIndexes: number[];
        if (typeof targetMapTileIndexes === 'number') {
            _targetMapTileIndexes = [ targetMapTileIndexes ];
        } else {
            _targetMapTileIndexes = targetMapTileIndexes as number[];
            if (!_targetMapTileIndexes || _targetMapTileIndexes.length === 0) {
                return(false);   //sanity checks
            }
        }

        const DURATION = PlayerTokenMover._MOVE_DURATION_MS;
        const DELAY = PlayerTokenMover._MOVE_DURATION_DELAY_MS;
        this._currentTweenData.targetPoint = null;
        this._currentTweenData.index = 0;
        this._currentTweenData.duration = DURATION;
        this._currentTweenData.delay = DELAY;
        this._currentTweenData.playerColor = color;
        this._currentTweenData.targetMapTileIndexes = _targetMapTileIndexes;
        this._currentTweenData.useCompleteCallback = useCompleteCallback;
        this._createMoveTween(token);

        return(true);
    }

    moveToOrigin(color: PlayerColors, useCompleteCallback: boolean = true): boolean {
        let token = this.board.getPlayerToken(color);
        if (!token) {
            return(false);  //sanity check
        }

        const tilePoint =
            this.board.mapData.tokenOriginTilePointsByPlayerColor.get(color);
        const pixelsPoint = BoardMath.TilePointToPixels(this.board.mapData, tilePoint);

        const DURATION = PlayerTokenMover._MOVE_TO_ORIGIN_DURATION_MS;
        this._currentTweenData.duration = DURATION
        this._currentTweenData.targetPoint = pixelsPoint;
        this._currentTweenData.targetMapTileIndexes = null;
        this._currentTweenData.useCompleteCallback = useCompleteCallback;
        this._createMoveTween(token);
        return(true);
    }

    stop() {
        if (this._tween) {
            this._tween.stop();
            this._tween = null;
        }
    }

    /**
     * @private
     */
    _createMoveTween(token: Phaser.Image) {
        this._tween = this.game.tweens.create(token);
        this._tween.onComplete.addOnce(this._onTweenCompleteMove, this);
        this._tween.onStart.addOnce(this._onTweenStartMove, this);
        
        this._createTokenTrail(token);

        const data = this._currentTweenData;
        const pixelsPoint = data.targetPoint ? 
            data.targetPoint :
            BoardMath.TileIndexToPixels(this.board.mapData,
                data.targetMapTileIndexes[data.index]);

        this._tween.to(
            {
                x: pixelsPoint.x,
                y: pixelsPoint.y
            },
            data.duration,
            Phaser.Easing.Linear.None,
            true,
            data.delay);
    }

    /**
     * @private
     */
    _createTokenTrail(token: Phaser.Image) {
        TokenTrailImage.Create(this.game, this._currentTweenData.playerColor,
            token);
    }

    /**
     * @private
     */
    _onTweenCompleteMove(target: Phaser.Image, tween: Phaser.Tween) {
        const data = this._currentTweenData;
        const mapTileIndexes = data.targetMapTileIndexes;
        const isLastTween =
            data.targetPoint !== null ||
            data.index + 1 === mapTileIndexes.length;
        
        if (isLastTween) {
            if (data.useCompleteCallback) {
                this.onMoveComplete.dispatch(this, data.playerColor);
            }
            return;
        }

        data.index++;
        this._createMoveTween(target);
    }

    /**
     * @private
     */
    _onTweenStartMove(target: Phaser.Image, tween: Phaser.Tween) {
        this.onTokenStep.dispatch(this, this._currentTweenData.playerColor);
    }
}
