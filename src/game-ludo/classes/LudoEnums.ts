export enum PlayerColors {
    BLUE = 'blue',
    RED = 'red',
    GREEN = 'green',
    YELLOW = 'yellow'
};
