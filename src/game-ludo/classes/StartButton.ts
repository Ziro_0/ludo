export default class StartButton extends Phaser.Group {
    game: Phaser.Game;
    onPressed: Phaser.Signal;

    constructor(game: Phaser.Game) {
        super(game);
        this.game = game;
        this._createButtonImage();
        this.onPressed = new Phaser.Signal();
    }

    destroy() {
      this.onPressed.dispose();
      super.destroy();
    }

    /**
     * @private
     */
    _createBackgroundImage(): Phaser.Image {
        const world = this.game.world;
        let image = this.game.add.image(
            world.centerX,
            world.height * 0.8,
            'play-button-background',
            null,
            this);
        image.anchor.set(0.5);
        image.width = world.width * 0.3;
        image.scale.y = image.scale.x;
        return(image);
    }

    /**
     * @private
     */
    _onPlayButtonPressed() {
        this.onPressed.dispatch();
    }

    _createButtonImage() {
        let buttonBg = this._createBackgroundImage();

        let buttonImage = this.game.add.image(
            buttonBg.x,
            buttonBg.y,
            'button-intro-play',
            null,
            this);

        buttonImage.inputEnabled = true;
        buttonImage.events.onInputDown.add(this._onPlayButtonPressed, this);
        buttonImage.anchor.set(0.5);
        buttonImage.scale.set(buttonBg.scale.x * 0.95);

        this.game.add.tween(buttonImage.scale)
            .to({
                x: buttonBg.scale.x,
                y: buttonBg.scale.x
            }, 500,
            Phaser.Easing.Sinusoidal.Out,
            true,
            0,
            -1,
            true);
            
    }
}
