import { PlayerColors } from "./LudoEnums";

export default class PlayerData {
    tileIndexPath: number[];
    tileIndexPathArrayIndex: number;
    endPathStartIndex: number;
    isInSafeZone: boolean;

    _color: PlayerColors;
    _name: string;

    /**
     * @constructor
     */
    constructor(color: PlayerColors, name: string) {
        this._color = color;
        this._name = name;

        this.tileIndexPathArrayIndex = -1;
    }

    /**
     * @readonly
     */
    get color(): PlayerColors {
        return(this._color);
    }

    get isInEndPath(): boolean {
        return(this.tileIndexPathArrayIndex >= this.endPathStartIndex);
    }

    /**
     * @readonly
     */
    get name(): string {
        return(this._name);
    }
}
