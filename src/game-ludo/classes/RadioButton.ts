export default class RadioButton extends Phaser.Image {
    game: Phaser.Game;
    onPressed: Phaser.Signal;

    _offKey: string;
    _onKey: string;
    _isOn: boolean;

    constructor(game: Phaser.Game, x: number, y: number, offKey: string,
    onKey: string) {
        super(game, x, y, offKey);

        this.game = game;
        this._offKey = offKey;
        this._onKey = onKey;

        this.inputEnabled = true;
        this.events.onInputDown.add(this._onInputDown, this);

        this.onPressed = new Phaser.Signal();
    }

    destroy() {
      this.onPressed.dispose();
      super.destroy();
    }

    get isOn(): boolean {
        return(this._isOn);
    }

    set isOn(value: boolean) {
        this._isOn = value;
        this.loadTexture(this._isOn ? this._onKey : this._offKey);
    }

    /**
     * @private
     */
    _onInputDown() {
        if (!this.isOn) {
            this.isOn = true;
            this.onPressed.dispatch();
        }
    }
}
