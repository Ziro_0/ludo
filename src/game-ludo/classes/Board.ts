import MapData from "./MapData";
import { PlayerColors } from "./LudoEnums";
import BoardMath from "./BoardMath";
import PlayerData from "./PlayerData";

export default class Board  {
    game: Phaser.Game;
    group: Phaser.Group;
    mapImageGroup: Phaser.Group;
    frameImage: Phaser.Image;
    mapData: MapData;
    
    _playersTokensByColor: Map<PlayerColors, Phaser.Image>;
    _playersNameBmpTextsByColor: Map<PlayerColors, Phaser.BitmapText>;

    /**
     * @constructor
     */
    constructor(game: Phaser.Game) {
        this._playersTokensByColor = new Map();
        this._playersNameBmpTextsByColor = new Map();

        this.game = game;

        this.group = this.game.add.group();
        this.mapImageGroup = this.game.add.group();

        this._initBoardImage();
        this._initMapData();
        this._initSafeZoneImages();

        //must add the child group to the parent group AFTER the child
        // has been setup, for some reason...
        this.group.add(this.mapImageGroup);
    }

    getPlayerToken(color: PlayerColors): Phaser.Image {
        return(this._playersTokensByColor.get(color));
    }

    setupPlayer(playerData: PlayerData, tokenIndex: number = 1) {
        const color: PlayerColors = playerData.color;
        this._createPlayerToken(color, tokenIndex);
        this._createPlayerName(playerData);
    }
    
    updatePlayerName(playerData: PlayerData) {
        const bitmapText = this._playersNameBmpTextsByColor.get(playerData.color);
        if (bitmapText) {
            bitmapText.text = playerData.name;
        }
    }

    /**
     * @private
     */
    _createPlayerName(playerData: PlayerData) {
        var rect = this.mapData.playerNameTextAreasByPlayerColor.get(playerData.color);
        const bitmapText = this.game.add.bitmapText(rect.x, rect.y, 'modenine',
            playerData.name, rect.height, this.mapImageGroup);
        this._playersNameBmpTextsByColor.set(playerData.color, bitmapText);
    }

    /**
     * @private
     */
    _createPlayerToken(color: PlayerColors, tokenIndex: number) {
        const mapTilePoint: Phaser.Point =
            this.mapData.tokenOriginTilePointsByPlayerColor.get(color);

        if (!mapTilePoint) {
            console.log("warning: invalid color: " + color);
            return;
        }

        const mapTilePixelsPoint: Phaser.Point =
            BoardMath.TilePointToPixels(this.mapData, mapTilePoint);
        
        let playerToken: Phaser.Image = this.game.add.image(
            mapTilePixelsPoint.x,
            mapTilePixelsPoint.y,
            this._getTokenImageKey(color, tokenIndex),
            null,
            this.mapImageGroup);

        playerToken.width = this.mapData.tilesize;
        playerToken.scale.y = playerToken.scale.x;
        playerToken.anchor.set(0.477, 0.433);

        this._playersTokensByColor.set(color, playerToken);
    }

    /**
     * @private
     */
    _getTokenImageKey(color: PlayerColors, index: number) {
        return('token-' + color + '-' + index);
    }

    /**
     * @private
     */
    _initBoardImage() {
        this.frameImage = this.game.add.image(0, 0, 'board-back-frame',
            null, this.group);
        this.game.add.image(0, 0, 'board', null, this.mapImageGroup);
        this.mapImageGroup.alignIn(this.frameImage, Phaser.CENTER);
    }

    /**
     * @private
     */
    _initMapData() {
        const jMapData = this.game.cache.getJSON('map-data');
        this.mapData = new MapData(jMapData);
    }

    /**
     * @private
     */
    _initSafeZoneImages() {
        this.mapData.safeZoneIconTileIndexes.forEach(function(tileIndex) {
            const pixelsPoint = BoardMath.TileIndexToPixels(this.mapData, tileIndex);
            let image = this.game.add.image(pixelsPoint.x, pixelsPoint.y,
                'safe-zone-icon', null, this.mapImageGroup);
            image.anchor.set(0.5);
        }, this);
    }
}
