import MapData from "./MapData";
import { PlayerColors } from "./LudoEnums";
import PlayerData from "./PlayerData";

export default class BoardMath {
    /**
     * @name BoardMath.BuildPlayerTileIndexPath
     */
    static BuildPlayerTileIndexPath(mapData: MapData, color: PlayerColors): object {
        const pathData = {
            tileIndexPath: [],
            endPathStartIndex: 0
        };

        let tileIndexPath: number[] = pathData.tileIndexPath;

        //start with the start index
        let tileIndex = mapData.tokenStartTileIndexesByPlayerColor.get(color);
        tileIndexPath.push(tileIndex);

        //build path around board
        const numTilesInPerimeterPath = mapData.numTilesInPerimeterPath;
        const tokenFinishPathTileIndexes =
            mapData.tokenFinishPathTileIndexesByPlayerColor.get(color);

        tileIndex = (tileIndex + 1) % numTilesInPerimeterPath;
        while (tileIndex !== tokenFinishPathTileIndexes[0]) {
            tileIndexPath.push(tileIndex);
            tileIndex = (tileIndex + 1) % numTilesInPerimeterPath;
        }

        pathData.endPathStartIndex = tileIndexPath.length + 1;

        //build finish path
        tokenFinishPathTileIndexes.forEach(function(finishPathTileIndex) {
            tileIndexPath.push(finishPathTileIndex);
        });

        return(pathData);
    }
    
    /**
     * @name BoardMath.TileIndexToPixels
     */
    static TileIndexToPixels(mapData: MapData, tileIndex: number): Phaser.Point {
        return(BoardMath.TilePointToPixels(mapData,
            mapData.tokenPathTilePoints[tileIndex]));
    }

    /**
     * @name BoardMath.TilePointToPixels
     */
    static TilePointToPixels(mapData: MapData, tilePoint: Phaser.Point): Phaser.Point {
        const tilesize: number = mapData.tilesize;
        const halfTilesize = tilesize * 0.5;
        return(new Phaser.Point(
            tilePoint.x * tilesize + halfTilesize,
            tilePoint.y * tilesize + halfTilesize));
    }
}
