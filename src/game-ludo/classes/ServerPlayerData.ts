import { PlayerColors } from "./LudoEnums";

export default class ServerPlayerData {
    /**
     * Individual path of tile indexes for this player
     * @type {number[]}
     */
    tileIndexPath: number[] = [];

    
    /**
     * Current array index position within tileIndexPath. A value of -1
     * means this player is still at the origin.
     * @type {number}
     */
    tileIndexPathArrayIndex: number = -1;
    
    /**
     * Array index within position within tileIndexPath that specifies
     * the first tile index when the player has reach the end path (this is
     * the first tile after the players goes past their own arrow towards
     * the map center)
     * @type {number}
     */
    endPathStartIndex: number = 0;

    /**
     * Determines if this player is in a safe zone
     * @type {boolean}
     */
    isSafe: boolean = false;
    
    /**
     * Name of the player. No name will default to "Player [playerIndex + 1]"
     * @type {string}
     */
    name: string = '';

    /**
     * This player's color
     * @type {PlayerColors}
     */
    color: PlayerColors;

    /**
     * @constructor
     */
    constructor() {
    }
}
