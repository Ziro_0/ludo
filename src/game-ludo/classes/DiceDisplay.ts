import { irandomRange } from '../util/util';

export default class DiceDisplay extends Phaser.Image {
    onInputDown: Phaser.Signal;
    onRollCompleted: Phaser.Signal;
    valueAfterRoll: number;

    _diceRollTextures: string[];
    _timer: Phaser.Timer;
    _rollTextureIndex: number;
    _timerEvent: Phaser.TimerEvent;

    constructor(game: Phaser.Game) {
        super(game, 0, 0, 'dice-1');

        this.anchor.set(0.5);
        this._setIdleSize();
        
        this._diceRollTextures = [
            'dice-roll-0',
            'dice-roll-1',
            'dice-roll-2'
        ];

        this._initTimer();

        this.inputEnabled = true;
        this.events.onInputDown.add(this._onInputDown, this);

        this.onInputDown = new Phaser.Signal();
        this.onRollCompleted = new Phaser.Signal();
    }

    destroy() {
        this._uninitTimer();
        this.onRollCompleted.dispose();
        this.onInputDown.dispose();

        super.destroy();
    }

    roll(value?: number) {
        if (value === undefined) {
            this.valueAfterRoll = irandomRange(1, 6);
        } else {
            this.valueAfterRoll = value;
        }

        this._rollTextureIndex = 0;
        this._startTimerEvent();

        this.game.sound.play('dice');
    }

    set value(points: number) {
        this.loadTexture('dice-' + points);
        this._setIdleSize();
        
        if (this._timerEvent) {
            this._timer.remove(this._timerEvent);
            this._timerEvent = null;
        }
    }

    /**
     * @private
     */
    _initTimer() {
        const AUTO_DESTROY = false;
        this._timer = this.game.time.create(AUTO_DESTROY);
        this._timer.onComplete.add(this._onTimerComplete, this);
        this._timer.start();
    }

    /**
     * @private
     */
    _onInputDown() {
        this.onInputDown.dispatch();
    }

    /**
     * @private
     */
    _onTimer() {
        this._rollTextureIndex = (this._rollTextureIndex + 1) %
            this._diceRollTextures.length;
        const texture = this._diceRollTextures[this._rollTextureIndex];
        this.loadTexture(texture);
        this._setRollingSize();
    }

    /**
     * @private
     */
    _onTimerComplete() {
        this.value = this.valueAfterRoll;
        this.onRollCompleted.dispatch(this.valueAfterRoll);
    }

    /**
     * @private
     */
    _setIdleSize() {
        this._setSize(88);
    }

    _setRollingSize() {
        this._setSize(110);
    }

    /**
     * @private
     */
    _setSize(size: number) {
        this.width = size;
        this.scale.y = this.scale.x;
    }

    /**
     * @private
     */
    _startTimerEvent() {
        this._timer.remove(this._timerEvent);
        this._timerEvent = this._timer.repeat(30, 20, this._onTimer, this);
    }

    /**
     * @private
     */
    _uninitTimer() {
        if (this._timer) {
            this._timer.destroy();
            this._timer = null;
        }
    }
}
