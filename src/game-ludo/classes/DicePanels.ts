import Board from "./Board";
import { PlayerColors } from "./LudoEnums";
import DiceDisplay from "./DiceDisplay";

export default class DicePanels {
    game: Phaser.Game;
    onDicePressed: Phaser.Signal;
    onRollCompleted: Phaser.Signal;
    board: Board;

    _diceDisplay: DiceDisplay;
    
    //key: PlayerColors
    //data: Phaser.Image
    _panelImages: object;

    constructor(game: Phaser.Game, board: Board) {
        this.game = game;
        this.board = board;

        this.onDicePressed = new Phaser.Signal();
        this.onRollCompleted = new Phaser.Signal();

        this._createPanelImages();
        this._createDiceDisplay();
    }

    destroy() {
        this.onDicePressed.dispose();
        this.onRollCompleted.dispose();
        this._diceDisplay.destroy();
    }
    
    set diceValue(value: number) {
        this._diceDisplay.value = value;
    }

    getPanelImage(color: PlayerColors): Phaser.Image {
        return(this._panelImages[color]);
    }

    present(color: PlayerColors) {
        this._showExclusiveImage(color);

        const panelImage: Phaser.Image = this._panelImages[color];
        this._diceDisplay.x = panelImage.x;
        this._diceDisplay.y = panelImage.y;
        this._diceDisplay.visible = true;
    }

    rollDice(value?: number) {
        this._diceDisplay.roll(value);
    }

    /**
     * @private
     */
    _createDiceDisplay() {
        this._diceDisplay = new DiceDisplay(this.game);
        this._diceDisplay.onInputDown.add(this._onDiceDisplayInputDown, this);
        this._diceDisplay.onRollCompleted.add(this._onDiceDisplayRollCompleted, this);
        this._diceDisplay.visible = false;
        this.game.world.add(this._diceDisplay);
    }

    /**
     * @private
     */
    _createPanel(color: PlayerColors, align: number) {
        let panelImage: Phaser.Image = this._createPanelImage(color);

        const mapGroup: Phaser.Group = this.board.group;
        const bounds: Phaser.Rectangle = new Phaser.Rectangle(
            mapGroup.x, mapGroup.y,
            mapGroup.width, mapGroup.height);
        panelImage.alignTo(bounds, align, 0, 10);

        this._panelImages[color] = panelImage;
    }

    /**
     * @private
     */
    _createPanelImage(color: PlayerColors): Phaser.Image {
        let image = this.game.add.image(0, 0, 'dice-bg-' + color);
        image.visible = false;
        image.anchor.set(0.5);
        return(image);
    }

    /**
     * @private
     */
    _createPanelImages() {
        this._panelImages = {};

        this._createPanel(PlayerColors.BLUE, Phaser.BOTTOM_LEFT);
        this._createPanel(PlayerColors.RED, Phaser.TOP_LEFT);
        this._createPanel(PlayerColors.GREEN, Phaser.TOP_RIGHT);
        this._createPanel(PlayerColors.YELLOW, Phaser.BOTTOM_RIGHT);
    }

    /**
     * @private
     */
    _onDiceDisplayInputDown() {
        this.onDicePressed.dispatch();
    }

    /**
     * @private
     */
    _onDiceDisplayRollCompleted(rollValue: number) {
        this.onRollCompleted.dispatch(rollValue);
    }
    
    /**
     * @private
     */
    _showExclusiveImage(color: PlayerColors) {
        Object.keys(this._panelImages).forEach(function(key: PlayerColors) {
            let image: Phaser.Image = this._panelImages[key];
            image.visible = key === color;
        }, this);
    }
}
