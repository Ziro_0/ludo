import { PlayerColors } from "./LudoEnums";

export default class MoveTweenData {
    targetMapTileIndexes: number[];
    
    playerColor: PlayerColors;

    targetPoint: Phaser.Point = null;

    index: number;
    duration: number;
    delay: number;

    useCompleteCallback: boolean;
}
