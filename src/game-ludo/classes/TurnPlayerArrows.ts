import { PlayerColors } from "./LudoEnums";
import DicePanels from "./DicePanels";

export default class TurnPlayerArrows {
    game: Phaser.Game;
    dicePanels: DicePanels;

    _presentTween: Phaser.Tween;

    //key: PlayerColors
    //data: Phaser.Image
    _arrowImages: object;

    constructor(game: Phaser.Game, dicePanels: DicePanels) {
        this.game = game;
        this.dicePanels = dicePanels;
        this._createArrowImages();
    }

    destroy() {
        this._destroyTween();
        this._destroyArrowImages();
    }

    present(color: PlayerColors) {
        this._destroyTween();

        let image = this._arrowImages[color];
        image.x = this._calcArrowImageStartX(color);

        this._presentTween = this.game.tweens.create(image);
        
        const scale = image.scale.x;
        const duration = 350;
        const offset = 15;
        this._presentTween.to({
            x: image.x - offset * (scale / Math.abs(scale))
        },
        duration,
        Phaser.Easing.Sinusoidal.InOut,
        true,
        0,
        -1,
        true);

        this._showExclusiveImage(color);
    }

    unpresent() {
        this._showExclusiveImage(null);
        this._destroyTween();
    }
    
    /**
     * @private
     */
    _calcArrowImageStartX(color: PlayerColors) {
        const dicePanelImage = this.dicePanels.getPanelImage(color);
        const arrowImage = this._arrowImages[color];
        const offsetScale = arrowImage.scale.x / Math.abs(arrowImage.scale.x);
        const offsetPosition = 10;
        return(
            dicePanelImage.x -
            (dicePanelImage.width * 0.5 * offsetScale) -
            (Math.abs(arrowImage.width) * 0.5 + offsetPosition) * offsetScale);
    }

    /**
     * @private
     */
    _createArrow(color: PlayerColors, align: number) {
        let arrowImage: Phaser.Image = this._createArrowImage(align);
        this._arrowImages[color] = arrowImage;
        arrowImage.x = this._calcArrowImageStartX(color);
        
        const dicePanelImage = this.dicePanels.getPanelImage(color);
        arrowImage.y = dicePanelImage.y;
    }

    /**
     * @private
     */
    _createArrowImage(align: number): Phaser.Image {
        let image = this.game.add.image(0, 0, 'turn-player-right-arrow');
        image.anchor.set(0.5);
        image.visible = false;
        if (align === Phaser.RIGHT) {
            image.scale.x = -image.scale.x;
        }
        return(image);
    }

    /**
     * @private
     */
    _createArrowImages() {
        this._arrowImages = {};

        this._createArrow(PlayerColors.BLUE, Phaser.RIGHT);
        this._createArrow(PlayerColors.RED, Phaser.RIGHT);
        this._createArrow(PlayerColors.GREEN, Phaser.LEFT);
        this._createArrow(PlayerColors.YELLOW, Phaser.LEFT);
    }

    _destroyArrowImages() {
        Object.keys(this._arrowImages).forEach(function(key: string) {
            let image: Phaser.Image = this._arrowImages[key];
            image.destroy();
            delete this._arrowImages[key];
        }, this);
    }

    /**
     * @private
     */
    _destroyTween() {
        if (this._presentTween) {
            this.game.tweens.remove(this._presentTween);
            this._presentTween = null;
        }
    }

    /**
     * @private
     */
    _showExclusiveImage(color: PlayerColors) {
        Object.keys(this._arrowImages).forEach(function(key: PlayerColors) {
            let image: Phaser.Image = this._arrowImages[key];
            image.visible = key === color;
        }, this);
    }
}
