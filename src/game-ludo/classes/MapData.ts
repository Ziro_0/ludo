import { PlayerColors } from "./LudoEnums";

export default class MapData {
    tokenFinishPathTileIndexesByPlayerColor: Map<PlayerColors, number[]>;
    playerNameTextAreasByPlayerColor: Map<PlayerColors, Phaser.Rectangle>;
    tokenOriginTilePointsByPlayerColor: Map<PlayerColors, Phaser.Point>;
    tokenStartTileIndexesByPlayerColor: Map<PlayerColors, number>;
    tokenPathTilePoints: Phaser.Point[];
    safeZoneTileIndexes: number[];
    safeZoneIconTileIndexes: number[];
    tilesize: number;
    numTilesInPerimeterPath: number;

    /**
     * @constructor
     */
    constructor(jsonMapData: object) {
        this.tilesize = this._initNumber(jsonMapData, 'tilesize');
        this.numTilesInPerimeterPath = this._initNumber(jsonMapData,
            'numTilesInPerimeterPath');

        this.playerNameTextAreasByPlayerColor = this._initRectsByPlayerColor(
            jsonMapData, "player-name-text-areas");

        this.tokenPathTilePoints = this._initArrayOfPoints(jsonMapData,
            'token-path-tile-points');
        
        this.tokenStartTileIndexesByPlayerColor = this._initNumbersByPlayerColor(
            jsonMapData, 'token-start-tile-indexes')
        
        this.tokenFinishPathTileIndexesByPlayerColor =
            this._initArrayOfNumbersByPlayerColor(
            jsonMapData, 'token-finish-path-tile-indexes');

        this.tokenOriginTilePointsByPlayerColor =
            this._initPointsByPlayerColor(jsonMapData, 'token-origin-tile-points');
        
        this.safeZoneTileIndexes = this._initArrayOfNumbers(jsonMapData,
            'safe-zone-tile-indexes');

        this.safeZoneIconTileIndexes = this._initArrayOfNumbers(jsonMapData,
            'safe-zone-icon-tile-indexes');
    }

    /**
     * @private
     */
    _initArrayOfNumbers(jsonMapData: object, key: string): number[] {
        return(jsonMapData[key]);
    }

    /**
     * @private
     */
    _initArrayOfNumbersByPlayerColor(jsonMapData: object,
    key: string):Map<PlayerColors, number[]> {
        const jData: object = jsonMapData[key];

        let data = new Map();
        data.set(PlayerColors.BLUE, jData[PlayerColors.BLUE]);
        data.set(PlayerColors.GREEN, jData[PlayerColors.GREEN]);
        data.set(PlayerColors.RED, jData[PlayerColors.RED]);
        data.set(PlayerColors.YELLOW, jData[PlayerColors.YELLOW]);
        return(data);
    }

    /**
     * @private
     */
    _initArrayOfPoints(jsonMapData: object, key: string): Phaser.Point[] {
        const jPoints: number[][] = jsonMapData[key];
        let points: Phaser.Point[] = [];

        jPoints.forEach(function(jPoint: number[]) {
            points.push(new Phaser.Point(jPoint[0], jPoint[1]));
        });

        return(points);
    }

    /**
     * @private
     */
    _initNumber(jsonMapData: object, key: string): number {
        return(jsonMapData[key]);
    }

    /**
     * @private
     */
    _initNumbersByPlayerColor(jsonMapData: object,
    key: string): Map<PlayerColors, number> {
        const jData: object = jsonMapData[key];
        
        let data = new Map();
        data.set(PlayerColors.BLUE,  jData[PlayerColors.BLUE]);
        data.set(PlayerColors.GREEN, jData[PlayerColors.GREEN]);
        data.set(PlayerColors.RED, jData[PlayerColors.RED]);
        data.set(PlayerColors.YELLOW, jData[PlayerColors.YELLOW]);
        return(data);
    }

    /**
     * @private
     */
    _initPointsByPlayerColor(jsonMapData: object,
    key: string): Map<PlayerColors, Phaser.Point> {
        const jData: any = jsonMapData[key];
        
        let data = new Map();
        for (const dataKey in jData) {
            const jPoint: number[] = jData[dataKey];
            data.set(dataKey, new Phaser.Point(jPoint[0], jPoint[1]));
        };
        return(data);
    }

    /**
     * @private
     */
    _initRectsByPlayerColor(jsonMapData: object,
    key: string): Map<PlayerColors, Phaser.Rectangle> {
        const jData: any = jsonMapData[key];
        
        let data = new Map();
        for (const dataKey in jData) {
            const jPoint: number[] = jData[dataKey];
            data.set(dataKey, new Phaser.Rectangle(jPoint[0], jPoint[1],
                jPoint[2], jPoint[3]));
        };
        return(data);
    }
}